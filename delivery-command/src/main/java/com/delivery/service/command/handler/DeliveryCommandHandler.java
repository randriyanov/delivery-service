package com.delivery.service.command.handler;

import com.delivery.service.mappers.DeliveryMapper;
import com.delivery.service.models.command.CreateDeliveryCommand;
import com.delivery.service.models.dto.DeliveryDetailsDto;
import com.delivery.service.models.dto.request.CreateDeliveryRequest;
import com.delivery.service.models.dto.request.ParcelCreateRequest;
import com.delivery.service.persistence.entity.Delivery;
import com.delivery.service.models.exception.address.AddressNotFoundException;
import com.delivery.service.persistence.repository.AddressRepository;
import com.delivery.service.persistence.repository.DeliveryRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DeliveryCommandHandler {

    private final DeliveryRepository deliveryRepository;
    private final AddressRepository addressRepository;
    private final DeliveryMapper deliveryMapper;

    @Transactional
    @CommandHandler
    public DeliveryDetailsDto createDelivery(CreateDeliveryCommand command) {
        CreateDeliveryRequest request = command.getCreateDeliveryRequest();
        var address = addressRepository.findAddressByAddressIdentifier(request.getAddressIdentifier())
                .orElseThrow(AddressNotFoundException::new);
        Delivery delivery = deliveryMapper.mapRequestToDelivery(request);
        delivery.setAddress(address);
        List<ParcelCreateRequest> parcelRequest = request.getParcelCreateRequests();
        delivery.setParcels(deliveryMapper.createParcelFromReq(parcelRequest, delivery));
        deliveryRepository.save(delivery);
        addressRepository.save(address);
        return deliveryMapper.mapDeliveryToDto(delivery);
    }
}
