package com.delivery.service.command.handler;

import com.delivery.service.models.exception.address.AddressAlreadyExistException;
import com.delivery.service.models.command.CreateAddressCommand;
import com.delivery.service.models.dto.request.AddressCreateRequest;
import com.delivery.service.models.dto.request.AddressCreateResponse;
import com.delivery.service.persistence.entity.Address;
import com.delivery.service.persistence.entity.AddressId;

import com.delivery.service.persistence.repository.AddressRepository;
import lombok.RequiredArgsConstructor;

import org.axonframework.commandhandling.CommandHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AddressCommandHandler {

    private final AddressRepository addressRepository;

    @Transactional
    @CommandHandler
    public AddressCreateResponse createNewAddress(CreateAddressCommand createAddressCommand) {
        AddressCreateRequest addressRequest = createAddressCommand.getAddressRequest();
        AddressId addressId = new AddressId();
        addressId.setCity(addressRequest.getCity());
        addressId.setFlatNumber(addressRequest.getFlatNumber());
        addressId.setHouseNumber(addressRequest.getHouseNumber());
        addressId.setStreet(addressRequest.getStreet());

        Address address = new Address();
        address.setAddressIdentifier(UUID.randomUUID().toString());
        address.setAddressId(addressId);
        if (addressRepository.findById(addressId).isPresent()) {
            throw new AddressAlreadyExistException();
        }
        addressRepository.save(address);
        return getAddressCreatedResponse(address.getAddressIdentifier());
    }

    private AddressCreateResponse getAddressCreatedResponse(String aggregateId) {
        return new AddressCreateResponse(aggregateId);
    }

}
