package com.delivery.service.mappers;

import com.delivery.service.models.dto.DeliveryDetailsDto;
import com.delivery.service.models.dto.ParcelDetailsDto;
import com.delivery.service.models.dto.request.CreateDeliveryRequest;
import com.delivery.service.models.dto.request.ParcelCreateRequest;
import com.delivery.service.persistence.entity.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class DeliveryMapper {

    public Delivery mapRequestToDelivery(CreateDeliveryRequest request) {
        var delivery = new Delivery();
        delivery.setTag(request.getTag());
        delivery.setStatus(DeliveryStatus.OPEN);
        return delivery;
    }

    public DeliveryDetailsDto mapDeliveryToDto(Delivery delivery) {
        var address = delivery.getAddress();
        var addressId = address.getAddressId();
        return new DeliveryDetailsDto(
                delivery.getDeliveryId(), address.getAddressIdentifier(), delivery.getTag(),
                addressId.getStreet(), addressId.getHouseNumber(), addressId.getCity(),
                addressId.getFlatNumber(), createParcelDetailsDtos(delivery));
    }

    public List<ParcelDetailsDto> createParcelDetailsDtos(Delivery delivery) {
        return delivery.getParcels().stream().map(parcel -> new ParcelDetailsDto(parcel.getId(),
                parcel.getTag())).collect(Collectors.toList());
    }

    public List<Parcel> createParcelFromReq(List<ParcelCreateRequest> parcelCreateRequests, Delivery delivery) {
        return parcelCreateRequests.stream().map(details -> {
            var parcel = new Parcel();
            parcel.setTag(details.getTag());
            parcel.setDelivery(delivery);
            return parcel;
        }).collect(Collectors.toList());
    }
}
