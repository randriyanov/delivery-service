package com.delivery.service.query;

import com.delivery.service.mappers.DeliveryMapper;
import com.delivery.service.models.dto.DeliveryDetailsDto;
import com.delivery.service.models.exception.delivery.DeliveryNotFoundException;
import com.delivery.service.models.query.GetDeliveryByIdQuery;
import com.delivery.service.persistence.entity.Delivery;
import com.delivery.service.persistence.repository.DeliveryRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeliveryQuery {

    private final DeliveryRepository deliveryRepository;
    private final DeliveryMapper deliveryMapper;

    @QueryHandler
    public DeliveryDetailsDto getDeliveryById(GetDeliveryByIdQuery query) {
        Delivery delivery = deliveryRepository.findDeliveryById(query.getDeliveryId())
                .orElseThrow(DeliveryNotFoundException::new);
        return deliveryMapper.mapDeliveryToDto(delivery);
    }
}
