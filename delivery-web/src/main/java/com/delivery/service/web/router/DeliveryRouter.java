package com.delivery.service.web.router;

import com.delivery.service.web.handler.DeliveryHandlerFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class DeliveryRouter {

    @Bean
    public RouterFunction<ServerResponse> routerDelivery(DeliveryHandlerFunction deliveryHandler) {
        return route(POST("/deliveries"), deliveryHandler::createDelivery)
                .andRoute(GET("/deliveries/{deliveryId}"), deliveryHandler::createDelivery);
    }
}
