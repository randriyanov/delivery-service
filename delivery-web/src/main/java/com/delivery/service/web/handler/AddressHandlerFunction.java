package com.delivery.service.web.handler;

import com.delivery.service.models.command.CreateAddressCommand;
import com.delivery.service.models.dto.request.AddressCreateRequest;
import com.delivery.service.models.dto.request.AddressCreateResponse;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


@Component
@RequiredArgsConstructor
public class AddressHandlerFunction {

    private final CommandGateway commandGateway;

    public Mono<ServerResponse> createAddress(ServerRequest request) {
        return request.bodyToMono(AddressCreateRequest.class)
                .flatMap(addressRequest ->
                        Mono.fromFuture(commandGateway.send(new CreateAddressCommand(addressRequest))))
                .flatMap(p -> ServerResponse.ok().body(Mono.just(p), AddressCreateResponse.class));
    }
}
