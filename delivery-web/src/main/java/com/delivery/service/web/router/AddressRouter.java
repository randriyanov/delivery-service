package com.delivery.service.web.router;

import com.delivery.service.web.handler.AddressHandlerFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class AddressRouter {

    @Bean
    public RouterFunction<ServerResponse> routerAddress(AddressHandlerFunction addressHandler) {
        return route(POST("/addresses"), addressHandler::createAddress);
    }
}
