package com.delivery.service.web.handler;

import com.delivery.service.models.command.CreateDeliveryCommand;
import com.delivery.service.models.dto.DeliveryDetailsDto;
import com.delivery.service.models.dto.request.CreateDeliveryRequest;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class DeliveryHandlerFunction {

    private final CommandGateway commandGateway;

    public Mono<ServerResponse> createDelivery(ServerRequest request) {
        return request.bodyToMono(CreateDeliveryRequest.class)
                .flatMap(addDeliveryRequest ->
                        Mono.fromFuture(commandGateway.send(
                                new CreateDeliveryCommand(addDeliveryRequest))))
                .flatMap(p -> ServerResponse.ok().body(Mono.just(p), DeliveryDetailsDto.class));
    }


}
