package com.delivery.service.persistence.repository;

import com.delivery.service.persistence.entity.Address;
import com.delivery.service.persistence.entity.AddressId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, AddressId> {

    Optional<Address> findAddressByAddressIdentifier(String addressIdentifier);
}
