package com.delivery.service.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "delivery")
@EqualsAndHashCode(of = {"deliveryId"})
public class Delivery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deliveryId;
    private String tag;
    @Enumerated
    @Column(columnDefinition = "smallint")
    private DeliveryStatus status;

    @OneToMany(
            mappedBy = "delivery",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Parcel> parcels = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Address address;
}
