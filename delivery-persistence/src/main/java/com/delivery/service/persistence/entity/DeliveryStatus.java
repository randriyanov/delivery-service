package com.delivery.service.persistence.entity;

public enum DeliveryStatus {
    OPEN,
    PROGRESS,
    DONE
}
