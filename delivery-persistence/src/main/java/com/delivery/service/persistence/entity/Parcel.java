package com.delivery.service.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "parcel")
@EqualsAndHashCode(of = {"id"})
public class Parcel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tag;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "deliveryId",
            referencedColumnName = "deliveryId"
    )
    private Delivery delivery;
}
