package com.delivery.service.persistence.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddressId implements Serializable {

    private String street;
    @Column(name = "house_number")
    private String houseNumber;
    private String city;
    @Column(name = "flat_number")
    private String flatNumber;
}
