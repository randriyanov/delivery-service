package com.delivery.service.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "address")
@EqualsAndHashCode(of = {"addressId"})
public class Address {

    @EmbeddedId
    private AddressId addressId;

    @Column(nullable = false, unique = true)
    private String addressIdentifier;

    @OneToMany(
            mappedBy = "address",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Delivery> deliveries = new ArrayList<>();

}
