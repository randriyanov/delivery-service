package com.delivery.service.persistence.repository;

import com.delivery.service.persistence.entity.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    @Query("FROM Delivery as d join fetch Parcel WHERE d.deliveryId=:deliveryId")
    Optional<Delivery> findDeliveryById(Long deliveryId);
}
