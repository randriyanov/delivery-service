package com.delivery.service.models.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GetDeliveryByIdQuery {
    private final Long deliveryId;
}
