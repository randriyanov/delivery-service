package com.delivery.service.models.dto.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParcelCreateRequest {
    private String tag;
}
