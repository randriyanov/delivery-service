package com.delivery.service.models.command;

import com.delivery.service.models.dto.request.AddressCreateRequest;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateAddressCommand {
    private AddressCreateRequest addressRequest;
}
