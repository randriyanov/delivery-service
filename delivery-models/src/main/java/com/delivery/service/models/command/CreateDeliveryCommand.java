package com.delivery.service.models.command;

import com.delivery.service.models.dto.request.CreateDeliveryRequest;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateDeliveryCommand {

    private CreateDeliveryRequest createDeliveryRequest;
}
