package com.delivery.service.models.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParcelDetailsDto {
    private Long parcelId;
    private String tag;
}
