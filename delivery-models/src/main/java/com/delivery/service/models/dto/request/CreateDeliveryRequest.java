package com.delivery.service.models.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CreateDeliveryRequest {
    @JsonProperty("address_id")
    private String addressIdentifier;
    private String tag;
    @JsonProperty("parcels")
    private List<ParcelCreateRequest> parcelCreateRequests;
}
