package com.delivery.service.models.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressCreateResponse {
    @JsonProperty("address_aggregate_identifier")
    private String addressAggregateIdentifier;
}
