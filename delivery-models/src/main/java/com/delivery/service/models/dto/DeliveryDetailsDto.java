package com.delivery.service.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryDetailsDto {
    @JsonProperty("delivery_id")
    private Long deliveryIdentifier;
    @JsonProperty("address_id")
    private String addressIdentifier;
    private String tag;
    private String street;
    @JsonProperty("house_number")
    private String houseNumber;
    private String city;
    @JsonProperty("flat_number")
    private String flatNumber;
    @JsonProperty("parcels")
    private List<ParcelDetailsDto> parcelDetails;
}
