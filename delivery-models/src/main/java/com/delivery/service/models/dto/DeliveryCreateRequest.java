package com.delivery.service.models.dto;

import com.delivery.service.models.dto.request.ParcelCreateRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DeliveryCreateRequest {
    private String addressIdentifier;
    private String tag;
    private List<ParcelCreateRequest> parcelCreateRequests = new ArrayList<>();
}
