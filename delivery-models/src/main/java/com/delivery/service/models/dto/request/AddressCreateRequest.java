package com.delivery.service.models.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressCreateRequest {

    private String street;
    private String houseNumber;
    private String city;
    private String flatNumber;
}
